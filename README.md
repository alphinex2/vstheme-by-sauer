# After Work Theme

![After Work Theme Image 2](https://gitlab.com/alphinex2/vstheme-by-sauer/raw/master/img/img2.png)

## Themes in this Extension
- Dark (Default)
- Dark Green Blue
- Experimental

## Description
This is Theme is made to have an simple consistent Color Theme for Visual Studio Code. The most noteable colors are the deep gray and blue.

## Officially Supported Languages:
- CSS
- JavaScript
- PHP
- HTML
- JSON
- SQL
- Go (Since 09.02.2019)
- XML (Since 12.02.2019)

Please notice, that the theme is in an very early release and will be improved with more support for other languages.

## Plans
* Adding old "Flat"-Theme:
> ![After Work Theme Image 1](https://gitlab.com/alphinex2/vstheme-by-sauer/raw/master/img/img1.png)
> ![After Work Theme Image 0](https://gitlab.com/alphinex2/vstheme-by-sauer/raw/master/img/img0.png)

## Please be informed
This Theme is in an **very early release** and does **not** support all lanuages, VSCode functions and code-Styles yet. There may be some issues when using the theme in focus of unreadable or inconsistent colors. Also the colors can change heavily.

## Issues, Advice and Requests
You can create new Issues, comment them or give me some advice at my Gitlab Repository: https://gitlab.com/alphinex2/vstheme-by-sauer
**Thank you in advance for this!**

---
 Thanks for give the Theme a try!