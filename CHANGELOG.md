# Changelog


## Version 0.4.0
###### 09.02.2019
- **ADDED** XML Support
- **ADDED** Experimental Theme
- **RECOLORED** Title Bar
- **FIXED** Badge Colors
- **RENAMED** *Dark Blue* theme to *Dark Green Blue*
> Experimental Theme is only for testing. May change heavily over time.

---
<sup>_Older changes_</sup>

## Version 0.3.0
###### 09.02.2019
- **ADDED** Go Support
- **ADDED** "Dark Blue"-Theme
- **ADDED** Namespace Support for PHP
- **ADDED** Class Support for JavaScript

## Version 0.2.0
###### 12.03.2018
- **ADDED** SQL Support
- **RECOLORED** Numbers
- **RECOLORED** Classes
- **RECOLORED** Very special Chars
- **OPTIMIZED** Colors for variables
- **FIXED** 'typeof' control in JavaScript not colored
- **FIXED** Brackets in PHP at some special conditions have wrong color
- **FIXED** 'catch', 'continue' & 'break' control in PHP not colored
- **FIXED** Single quoted strings in JavaScript arent colored
- **FIXED** Single quoted strings in HTML arent colored

## Version 0.1.1
###### 10.03.2018
- **RECOLORED** Active Selection in Lists
- **RECOLORED** Scrollbar
- **RECOLORED** Search Highlighting in Lists for better readability
- **RECOLORED** Search Highlighting in Editor and Search-Results for better readability
- **RECOLORED** Badges
- **OPTIMIZED** Changed Accent Blue
- **OPTIMIZED** Colors in Diff Editor
- **FIXED** Colors under scrollbar not visibile
- **FIXED** Hover colors on Statusbar

## Version 0.1.0
###### 09.03.2018
- **ADDED** JSON Support
- **ADDED** HTML Support
- **RECOLORED** CSS
- **RECOLORED** JavaScript
- **RECOLORED** PHP
- **FIXED** Statusbar not readable when in Debug-Mode

## Version 0.0.2
###### 09.03.2018
- **FIXED** Selecting the Theme does not change anything.

## Version 0.0.1
- Initial release
